<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        # definimos los valores iniciales para nuestro calendario
        $month = date("n");
        $year = date("Y");
        $diaActual = date("j");
         
        # Devuelve 0 para domingo, 6 para sabado
        $diaSemana=date("w",mktime(0,0,0,$month,1,$year))+7;
        # Obtenemos el ultimo dia del mes
        $ultimoDiaMes=date("d",(mktime(0,0,0,$month+1,1,$year)-1));
         
        $meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio","Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

        $last_cell = $diaSemana + $ultimoDiaMes;

        return $this->render('home/index.html.twig', array(
            'year' => $year,
            'month' => array($meses[$month-1], $month-1),
            'diaSemana' => $diaSemana,
            'ultimoDiaMes' => $ultimoDiaMes,
            'diaActual' => $diaActual,
            'last_cell' => $last_cell
        ));
    }

    /**
     * @Route("/politica-de-cookies", name="cookies")
     */
    public function cookie()
    {
    	return $this->render('cookies.php.twig');
    }

    /**
    * @Route("/folder-css/{Name?null}", name="folder-css")
    * 
    */
    public function CssProject($Name)
    {
    	if($Name == 'null')
    		return $this->render('FolderCss.php.twig');
    	else
    		return $this->render('TemplateCss.php.twig', array (
    			'Name' => $Name
    		));
    }

    /**
    * @Route("/calendar", name="calendar")
    * 
    */
    public function calendar(){

    }
}
