/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// any CSS you require will output into a single css file (app.css in this case)

window.jQuery = window.$ = require('jquery');
import Popper from 'popper.js';
window.Popper = Popper;

require('../css/app.scss');
require('bootstrap-material-design');

console.log(window);

$(window).on('scroll', function() {

	console.log('Se desplaza');

	var barra = $(window).scrollTop();

	$('h1').text(barra);


});